package com.example.myapplicationfirebase;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.example.myapplicationfirebase.Module.CommunityItems;
import com.example.myapplicationfirebase.ViewHolder.MyCouponItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Container: RecyclcerView
 * Show My Coupon List(items)
 * **/

public class MycouponActivity extends AppCompatActivity {
    private RecyclerView.LayoutManager layoutManager;
    public ImageView item_favorite;
    FirebaseDatabase database;
    DatabaseReference tb_user;
    DatabaseReference tb_category_items;
    RecyclerView my_coupon_list;
    final String[] re_v = new String[1];
    FirebaseRecyclerAdapter<CategoryItems, MyCouponItemViewHolder> adapter;

    Dialog popAddPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycoupon);

        popAddPost = new Dialog(this);
        popAddPost.setContentView(R.layout.dialog_signin);
        popAddPost.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popAddPost.getWindow().getAttributes().gravity = Gravity.TOP;

        my_coupon_list = (RecyclerView) findViewById(R.id.my_coupon_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_user = database.getReference("User");
        tb_category_items = database.getReference("Category Items");
        layoutManager = new LinearLayoutManager(this);
        my_coupon_list.setLayoutManager(layoutManager);


        layoutManager = new LinearLayoutManager(this);
        my_coupon_list.setLayoutManager(layoutManager);

        getMyCouponList();

    }

    private void getMyCouponList() {
        int[] categoryNum = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        adapter = new FirebaseRecyclerAdapter<CategoryItems, MyCouponItemViewHolder>(
                CategoryItems.class,
                R.layout.activity_my_coupon_items,
                MyCouponItemViewHolder.class,
                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list")) {
            @Override
            protected void populateViewHolder(MyCouponItemViewHolder myCouponItemViewHolder, final CategoryItems model, int i) {
                myCouponItemViewHolder.item_name.setText(model.getName());
                myCouponItemViewHolder.item_description.setText(model.getDescription());
                myCouponItemViewHolder.item_code.setText(model.getCode_number());

                //Count user favorite number
                categoryNum[Integer.parseInt(model.getCategoryId())]++;

                if(model.getImage().indexOf("https") == 0) {
                    Picasso.with(getBaseContext()).load(model.getImage()).into(myCouponItemViewHolder.item_img);
                } else {
                    int resId = getResources().getIdentifier(model.getImage(), "drawable", getPackageName());
                    myCouponItemViewHolder.item_img.setImageResource(resId);
                }
                myCouponItemViewHolder.item_del.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Map<String, Object> map = new HashMap<>();
                        tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(model.getItemId()).removeValue();
                        tb_category_items.child(model.getItemId()).child("favorite").setValue("false");
                        tb_user.updateChildren(map);
                    }
                });
                myCouponItemViewHolder.item_share.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        String body = model.getName();
                        String sub = model.getCode_number();
                        intent.putExtra(Intent.EXTRA_SUBJECT, body);
                        intent.putExtra(Intent.EXTRA_TEXT, sub);
                        startActivity(Intent.createChooser(intent, "Share Coupon!"));
                    }
                });
            }
        };
        my_coupon_list.setAdapter(adapter);

        final Handler handler  = new Handler();
        final Runnable runnable = () -> {
            re_v[0] = WeightedAriMean.getWeightedAriMean(categoryNum);
            iniPost(re_v[0]);
        };
        handler.postDelayed(runnable, 1000);
    }
    String categoryId = "0";
    String code_number, image, name, itemId, description;
    private void iniPost(String s) {
        popAddPost.show();
        final TextView item_name = popAddPost.findViewById(R.id.item_name);
        final TextView item_code = popAddPost.findViewById(R.id.item_code);
        final ImageView item_img = popAddPost.findViewById(R.id.item_img);;
        item_favorite = (ImageView) popAddPost.findViewById(R.id.item_favorite);

        int min = 0;
        int max = 9;
        final int[] counter = {0};
        Random r = new Random();
        final int i1 = r.nextInt(max - min + 1) + min;
        //Log.i("==============recommandValue s===========", s);

        //tb_category_items.child(String.valueOf(i1)).addListenerForSingleValueEvent(new ValueEventListener() {
        tb_category_items.orderByChild("categoryId").equalTo(s).addValueEventListener(new ValueEventListener()  {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot data: dataSnapshot.getChildren()) {
                    //Log.i("==============items.getName() ===========", counter[0] + "==" + i1);
                    if (counter[0] == i1) {
                        categoryId = data.child("categoryId").getValue().toString();
                        code_number = data.child("code_number").getValue().toString();
                        image = data.child("image").getValue().toString();
                        name = data.child("name").getValue().toString();
                        itemId = data.child("itemId").getValue().toString();
                        description = data.child("description").getValue().toString();
                        break;
                    }
                    counter[0]++;
                }
                CategoryItems items = dataSnapshot.getValue(CategoryItems.class);
                item_name.setText(name);
                item_code.setText(code_number);
                Picasso.with(getBaseContext()).load(image).into(item_img);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                //Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        item_favorite.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Map<String, Object> map = new HashMap<>();
                String id = UUID.randomUUID().toString();
                CategoryItems items = new CategoryItems(categoryId,
                        code_number,
                        image,
                        name,
                        "true",
                        itemId,
                        description);
                item_favorite.setImageResource(R.drawable.ic_favorite);
                tb_category_items.child(itemId).child("favorite").setValue("true");
                //Log.i("===============String.valueOf(i1)==============", String.valueOf(i1));
                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(itemId).setValue(items);
                //Log.i("===============itemId==============", itemId);


                tb_category_items.updateChildren(map);
                tb_user.updateChildren(map);

                popAddPost.dismiss();
            }
        });
    }
}