package com.example.myapplicationfirebase;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class ScancodeActivity extends AppCompatActivity {
    //========================== Scan QR Code ==========================
    SurfaceView surfaceView;
    TextView qr_text;
    //CameraSource cameraSource;
    //BarcodeDetector barcodeDetector;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if (result!= null)
        {
            if (result.getContents()==null)
            {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this,result.getContents(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, AddQRcodInfo.class);
                intent.putExtra("qrCodeInfo", result.getContents());
                startActivity(intent);

            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scancode);
    }


    public void scanBarCode(View v){
        //Intent intent = new Intent(this, ProductAdditionActivity.class);
        //startActivity(intent);
        //Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        //if(getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).size() == 0) {
        //    Log.i("==========Info====", "not install");
        //} else {
                    //選擇SCAN_MODE
                    //只判斷QRCode
                    //intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    //只判斷二維條碼
           //         intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
                    //支援的調碼都可以判斷
                    //intent.putExtra("SCAN_MODE","SCAN_MODE");
                    //呼叫ZXing Scanner，完成動作後回傳1
          //          startActivityForResult(intent, 1);

        //}
        try {
            final Activity activity = this;
            IntentIntegrator integrator = new IntentIntegrator(activity);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setPrompt("Scan a barcode");
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.initiateScan();
        }catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            Log.i("===========scan", "scan nothing");
        }
    }

    public void scanQRCode(View v){
        final Activity activity = this;
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    public void keyinCode(View v){
        Intent intent = new Intent(this, AddSerialNumber.class);
        startActivity(intent);
    }

}