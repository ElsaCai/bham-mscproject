package com.example.myapplicationfirebase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Module.CommunityItems;

import java.util.ArrayList;

public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CommunityItems> communityList;
    private LayoutInflater layoutInflater;

    public CommunityAdapter(ArrayList<CommunityItems> communityList, Context context) {
        this.communityList = communityList;
        this.context = context;
    }
    @NonNull
    @Override
    public CommunityAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.community_item, parent, false);
        return new CommunityAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommunityAdapter.ViewHolder holder, int position) {
        final CommunityItems item = (CommunityItems) communityList.get(position);
        //holder.textView_username.setText(item.getUsername());
        //holder.textView_topic.setText(item.getTitle());
        //holder.textView_content.setText(item.getContent());
    }

    @Override
    public int getItemCount() {
        return communityList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        /*private TextView textView_topic;
        private TextView textView_username;
        private TextView textView_content;*/


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            /*textView_topic = (TextView) itemView.findViewById(R.id.item_topic);
            textView_content = (TextView) itemView.findViewById(R.id.item_content);
            textView_username = (TextView) itemView.findViewById(R.id.item_username);*/
        }
    }
}
