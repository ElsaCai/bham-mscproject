package com.example.myapplicationfirebase;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.example.myapplicationfirebase.Module.MyCouponItems;

public class MyCouponAdapter extends RecyclerView.Adapter<MyCouponAdapter.ViewHolder> {
    private ArrayList<MyCouponItems> my_products;
    private Context context;
    private LayoutInflater layoutInflater;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onDeleteClick(int position);
        void onShareClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) { mListener = listener; }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView_name;
        private TextView textView_code;
        private ImageView imageView_pr;
        private ImageView imageView_del;
        private ImageView imageView_share;

        public ViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            textView_name = (TextView) itemView.findViewById(R.id.item_name);
            textView_code = (TextView) itemView.findViewById(R.id.item_code);
            imageView_pr = (ImageView) itemView.findViewById(R.id.item_img);
            imageView_del = (ImageView) itemView.findViewById(R.id.item_del);
            imageView_share = (ImageView) itemView.findViewById(R.id.item_share);

            imageView_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onShareClick(position);
                        }
                    }
                }
            });
            imageView_del.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View view) {
                    if(listener != null) {
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION) {
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }

    public MyCouponAdapter(ArrayList<MyCouponItems> my_products, Context context) {
        this.my_products = my_products;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.activity_my_coupon_items, parent, false);
        return new ViewHolder(itemView, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MyCouponItems item = (MyCouponItems) my_products.get(position);
        //holder.imageView_pr.setImageResource(item.getImage());
        holder.textView_name.setText(item.getName());
        holder.textView_code.setText(item.getCode_number());
    }

    @Override
    public int getItemCount() {
        return my_products.size();
    }

}
