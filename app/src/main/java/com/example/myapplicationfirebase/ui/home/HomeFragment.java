package com.example.myapplicationfirebase.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myapplicationfirebase.CommunityActivity;
import com.example.myapplicationfirebase.CreditCardList;
import com.example.myapplicationfirebase.DiscoverCouponActivity;
import com.example.myapplicationfirebase.MycouponActivity;
import com.example.myapplicationfirebase.R;
import com.example.myapplicationfirebase.ScancodeActivity;

/**
 * Five CardView
 */
public class HomeFragment extends Fragment {

    //<========================== CardView ==========================>
    CardView my_coupon;
    CardView community;
    CardView scan_code;
    CardView discover_coupon;
    CardView card_info;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        /*final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        //========================== CardView ==========================
        my_coupon = root.findViewById(R.id.my_coupon);
        community = root.findViewById(R.id.community);
        scan_code = root.findViewById(R.id.scan_code);
        discover_coupon = root.findViewById(R.id.discover_coupon);
        card_info = root.findViewById(R.id.card_info);

        my_coupon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MycouponActivity.class);
                startActivity(intent);
            }
        });

        community.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CommunityActivity.class);
                startActivity(intent);
            }
        });

        scan_code.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScancodeActivity.class);
                startActivity(intent);
            }
        });

        discover_coupon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DiscoverCouponActivity.class);
                startActivity(intent);
            }
        });

        card_info.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreditCardList.class);
                startActivity(intent);
            }
        });

        return root;
    }
}