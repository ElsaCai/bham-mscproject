package com.example.myapplicationfirebase;

import android.util.Log;

import com.example.myapplicationfirebase.Module.CreditCardInfoItems;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class CreditCardInfo {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference db_credit_items = database.getReference("Credit Items");

    void loadCreditCardData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONArray jsonArray = new JSONArray();
                try {
                    int i = 0, j=0;
                    Document doc_card = Jsoup.connect("https://www.cardrates.com/reviews/cash-back/").get();
                    //Get Brand Name
                    Elements el_card_img = doc_card.select("div.review-left > a.card-thumbnail > img");
                    for (Element element : el_card_img) {
                        String card_name = element.attr("alt").replace("Review", "");
                        String card_img = element.attr("src");
                        //Log.i("=====card_name====", card_name);
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("card_name", card_name);
                        jsonObject.put("card_img", card_img);
                        jsonArray.put(jsonObject);
                        i++;
                        if(i==10) break;
                    }
                    Elements el_card_info = doc_card.select("span.bullets-limited > ul > li:first-child");

                    for (Element element : el_card_info) {
                        String card_info = element.text();
                        JSONObject var1 = jsonArray.getJSONObject(j);
                        var1.put("card_info", card_info);
                        //Log.i("=====card_info====", card_info);
                        j++;
                        if(j==10) break;
                    }

                    for (int n = 0; n < jsonArray.length(); n++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(n);
                        String card_name = jsonObject.getString("card_name");
                        String card_info = jsonObject.getString("card_info");
                        String card_img = jsonObject.getString("card_img");

                        CreditCardInfoItems items = new CreditCardInfoItems(
                                card_name,
                                card_img,
                                card_info);
                        db_credit_items.child(String.valueOf(n)).setValue(items);
                    }

                } catch (IOException | JSONException e) {
                    //Log.i("===========title Error======", Objects.requireNonNull(e.getMessage()));
                }
            }
        }).start();
    }

}
