package com.example.myapplicationfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.ViewHolder.DiscoverCouponListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.example.myapplicationfirebase.Module.Category;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Container: RecyclerView
 * Show Coupon List(items)
 * **/
public class DiscoverCouponActivity extends AppCompatActivity {

    //private ArrayList<Category> products = new ArrayList<Category>();
    //private CategoryAdapter categoryAdapter;
    private RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<Category, DiscoverCouponListViewHolder> adapter;

    FirebaseDatabase database;
    DatabaseReference tb_discover_coupon;
    RecyclerView coupon_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_coupon);
        coupon_list = (RecyclerView) findViewById(R.id.coupon_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_discover_coupon = database.getReference("Category");

        layoutManager = new LinearLayoutManager(this);
        coupon_list.setLayoutManager(layoutManager);



        loadMenu();

    }

    private void loadMenu(){
        adapter = new FirebaseRecyclerAdapter<Category, DiscoverCouponListViewHolder>(
                        Category.class, R.layout.coupon_item, DiscoverCouponListViewHolder.class, tb_discover_coupon) {
                    @Override
                    protected void populateViewHolder(DiscoverCouponListViewHolder discoverCouponListViewHolder, Category model, int i) {
                        discoverCouponListViewHolder.txt_category_name.setText(model.getName());
                        //Log.i("=================Discover getName", model.getName());
                        int resId = getResources().getIdentifier(model.getImage(), "drawable", getPackageName());
                        //Log.i("=================Discover resId", String.valueOf(resId));
                        discoverCouponListViewHolder.list_image.setImageResource(resId);
                        final  Category clickItem = model;
                        discoverCouponListViewHolder.setItemClickListener(new ItemClickListener() {
                            @Override
                            public void onClick(View view, int position, boolean isLongClick) {
                                //Toast.makeText(DiscoverCouponActivity.this, ""+clickItem.getImage(), Toast.LENGTH_SHORT).show();
                                Intent activity_category_list = new Intent(DiscoverCouponActivity.this, CategoryListActivity.class);
                                activity_category_list.putExtra("categoryNum", adapter.getRef(position).getKey());
                                startActivity(activity_category_list);
                            }
                        });
                    }
                };
        coupon_list.setAdapter(adapter);
    }

}