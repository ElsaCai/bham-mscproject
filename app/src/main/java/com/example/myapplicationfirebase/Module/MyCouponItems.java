package com.example.myapplicationfirebase.Module;

public class MyCouponItems {
    private String name;
    private String image;
    private String code_number;
    private String brand;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCode_number() {
        return code_number;
    }

    public void setCode_number(String code_number) {
        this.code_number = code_number;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MyCouponItems(String name, String image, String code_number, String brand, String description) {
        this.name = name;
        this.image = image;
        this.code_number = code_number;
        this.brand = brand;
        this.description = description;
    }
}
