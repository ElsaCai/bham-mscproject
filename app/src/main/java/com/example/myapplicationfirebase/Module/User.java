package com.example.myapplicationfirebase.Module;

public class User {
    private String user_name;
    private String pwd;
    private String email;
    private String sex;
    private String country;
    private String age;

    public User(){}

    public User(String user_name, String pwd, String email, String sex, String country, String age) {
        this.user_name = user_name;
        this.pwd = pwd;
        this.email = email;
        this.sex = sex;
        this.country = country;
        this.age = age;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
