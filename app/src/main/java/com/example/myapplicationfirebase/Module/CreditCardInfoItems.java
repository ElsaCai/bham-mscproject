package com.example.myapplicationfirebase.Module;

public class CreditCardInfoItems {
    private String card_name, card_img, card_info;

    public CreditCardInfoItems(){}
    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getCard_img() {
        return card_img;
    }

    public void setCard_img(String card_img) {
        this.card_img = card_img;
    }

    public String getCard_info() {
        return card_info;
    }

    public void setCard_info(String card_info) {
        this.card_info = card_info;
    }

    public CreditCardInfoItems(String card_name, String card_img, String card_info) {
        this.card_name = card_name;
        this.card_img = card_img;
        this.card_info = card_info;
    }
}
