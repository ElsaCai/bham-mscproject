package com.example.myapplicationfirebase.Module;

public class CommunityItems {
    private String title;
    private String content;
    private String username;
    private String postId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CommunityItems(){}
    public CommunityItems(String title, String content, String username, String postId) {
        this.title = title;
        this.content = content;
        this.username = username;
        this.postId = postId;
    }
}
