package com.example.myapplicationfirebase.Module;

/**
 *  "Category Items": {
 *     "1": {
 *       "name": "Uniqlo",
 *       "image": "gitf",
 *       "product_name": "t-shirt",
 *       "code_number": "D11550",
 *       "description": "under 50%",
 *       "categoryId": "1",
 *       "favorite": "false"
 *     }
 *  }
 */
public class CategoryItems {
    private String categoryId, code_number, image, name, favorite, itemId, description; //description, product_name

    public CategoryItems(){}

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCode_number() {
        return code_number;
    }

    public void setCode_number(String code_number) {
        this.code_number = code_number;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public CategoryItems(String categoryId, String code_number, String image, String name, String favorite, String itemId, String description) {
        this.categoryId = categoryId;
        this.code_number = code_number;
        this.image = image;
        this.name = name;
        this.favorite = favorite;
        this.itemId = itemId;
        this.description = description;
    }
}
