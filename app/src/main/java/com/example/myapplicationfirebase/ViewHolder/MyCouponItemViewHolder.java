package com.example.myapplicationfirebase.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.R;

public class MyCouponItemViewHolder extends RecyclerView.ViewHolder {
    public TextView item_name;
    public TextView item_description;
    public TextView item_code;
    public ImageView item_del;
    public ImageView item_share;
    public ImageView item_img;

    public MyCouponItemViewHolder(@NonNull View itemView) {
        super(itemView);

        item_name = (TextView) itemView.findViewById(R.id.item_name);
        item_description = (TextView)itemView.findViewById(R.id.item_description);
        item_code = (TextView)itemView.findViewById(R.id.item_code);
        item_del = (ImageView) itemView.findViewById(R.id.item_del);
        item_share = (ImageView) itemView.findViewById(R.id.item_share);
        item_img = (ImageView) itemView.findViewById(R.id.item_img);
    }
}
