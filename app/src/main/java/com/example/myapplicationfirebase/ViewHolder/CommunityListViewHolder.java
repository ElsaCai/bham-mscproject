package com.example.myapplicationfirebase.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.R;

public class CommunityListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView community_item_topic;
    public TextView community_item_content;
    public TextView community_item_username;
    public ImageView community_item_person;

    private ItemClickListener itemClickListener;
    public CommunityListViewHolder(@NonNull View itemView) {
        super(itemView);

        community_item_topic = (TextView) itemView.findViewById(R.id.community_item_topic);
        community_item_content = (TextView)itemView.findViewById(R.id.community_item_content);
        community_item_username = (TextView) itemView.findViewById(R.id.community_item_username);
        community_item_person = (ImageView) itemView.findViewById(R.id.community_item_person);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}
