package com.example.myapplicationfirebase.ViewHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.R;

public class CommunityItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView community_item_username;
    public TextView body;

    public CommunityItemViewHolder(@NonNull View itemView) {
        super(itemView);
        community_item_username = (TextView) itemView.findViewById(R.id.community_item_username);
        body = (TextView)itemView.findViewById(R.id.body);
    }


    @Override
    public void onClick(View view) {

    }
}
