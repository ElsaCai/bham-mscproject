package com.example.myapplicationfirebase.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.R;

public class CreditCardItemViewHolder extends RecyclerView.ViewHolder{
    public TextView card_name;
    public TextView card_info;
    public ImageView card_img;

    public CreditCardItemViewHolder(@NonNull View itemView) {
        super(itemView);

        card_name = (TextView) itemView.findViewById(R.id.card_name);
        card_info = (TextView)itemView.findViewById(R.id.card_info);
        card_img = (ImageView) itemView.findViewById(R.id.card_img);
    }
}
