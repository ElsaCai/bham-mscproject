package com.example.myapplicationfirebase.ViewHolder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.R;

public class CategoryItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView category_item_name;
    public TextView category_item_discount;
    public TextView category_item_code;
    public ImageView category_item_img;
    public ImageView item_favorite;

    public CategoryItemViewHolder(@NonNull View itemView) {
        super(itemView);
        category_item_name = (TextView) itemView.findViewById(R.id.category_item_name);
        category_item_discount = (TextView) itemView.findViewById(R.id.category_item_discount);
        category_item_code = (TextView)itemView.findViewById(R.id.category_item_code);
        item_favorite = (ImageView) itemView.findViewById(R.id.item_favorite);
        category_item_img = (ImageView) itemView.findViewById(R.id.category_item_img);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

    }
}
