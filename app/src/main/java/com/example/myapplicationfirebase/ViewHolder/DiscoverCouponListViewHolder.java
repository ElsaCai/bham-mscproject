package com.example.myapplicationfirebase.ViewHolder;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.R;

public class DiscoverCouponListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_category_name;
    public ImageView list_image;

    private ItemClickListener itemClickListener;
    public DiscoverCouponListViewHolder(@NonNull View itemView) {
        super(itemView);
        //Log.i("UI >>>>>>>>>>>>>>", "DiscoverCouponListViewHolder");
        txt_category_name = (TextView)itemView.findViewById(R.id.txt_category_name);
        list_image = (ImageView)itemView.findViewById(R.id.list_image);

        itemView.setOnClickListener(this);

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
