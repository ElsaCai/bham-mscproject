package com.example.myapplicationfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.Module.Category;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.example.myapplicationfirebase.Module.MyCouponItems;
import com.example.myapplicationfirebase.Module.User;
import com.example.myapplicationfirebase.ViewHolder.CategoryItemViewHolder;
import com.example.myapplicationfirebase.ViewHolder.DiscoverCouponListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Container: RecyclcerView
 * Show Discover Coupon Item List for each Category
 * **/

public class CategoryListActivity extends AppCompatActivity {
    private CategoryItems items = new CategoryItems();
    private RecyclerView.LayoutManager layoutManager;
    String categoryNum = "";
    FirebaseDatabase database;
    DatabaseReference tb_category_items;
    DatabaseReference tb_user;
    RecyclerView category_list;
    FirebaseRecyclerAdapter<CategoryItems, CategoryItemViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        category_list = (RecyclerView) findViewById(R.id.category_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_category_items = database.getReference("Category Items");
        tb_user = database.getReference("User");
        layoutManager = new LinearLayoutManager(this);
        category_list.setLayoutManager(layoutManager);


        //Get Category Item by categoryId
        if(getIntent() != null)
            categoryNum = getIntent().getStringExtra("categoryNum");
        if(!categoryNum.isEmpty() && categoryNum != null)
            getCategoryItem(categoryNum);

    }

    private void getCategoryItem(String categoryId) {
        adapter = new FirebaseRecyclerAdapter<CategoryItems, CategoryItemViewHolder>(
                    CategoryItems.class,
                    R.layout.activity_category_items,
                    CategoryItemViewHolder.class,
                    tb_category_items.orderByChild("categoryId").equalTo(categoryNum)) {
                @Override
                protected void populateViewHolder(final CategoryItemViewHolder categoryItemViewHolder, final CategoryItems model, final int i) {
                    categoryItemViewHolder.category_item_name.setText(model.getName());
                    categoryItemViewHolder.category_item_discount.setText(model.getDescription());
                    categoryItemViewHolder.category_item_code.setText(model.getCode_number());
                    //Log.i("=================Discover getName", model.getName());
                //    int resId = getResources().getIdentifier(model.getImage(), "drawable", getPackageName());
                    //Log.i("=================Discover resId", String.valueOf(Common.currentUser));
                //    categoryItemViewHolder.category_item_img.setImageResource(resId);
                    if(model.getImage().indexOf("https") == 0) {
                        Picasso.with(getBaseContext()).load(model.getImage()).into(categoryItemViewHolder.category_item_img);
                    }

                    if(/*model.getFavorite().equals(Common.currentUser.getEmail())*/model.getFavorite().equals("true")) {
                        categoryItemViewHolder.item_favorite.setImageResource(R.drawable.ic_favorite);
                    } else {
                        categoryItemViewHolder.item_favorite.setImageResource(R.drawable.ic_favorite_border);
                    }
                    categoryItemViewHolder.item_favorite.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                            Map<String, Object> map = new HashMap<>();
                            if(model.getFavorite().equals("false")) {
                                categoryItemViewHolder.item_favorite.setImageResource(R.drawable.ic_favorite);
                                //map.put("favorite", Common.currentUser.getEmail());
                                //tb_category_items.child(model.getItemId()).child("favorite").setValue(Common.currentUser.getEmail());
                                tb_category_items.child(model.getItemId()).child("favorite").setValue("true");
                                //>>>>>>>>>>>>> Add favorite_list into current user
                                CategoryItems items = new CategoryItems(model.getCategoryId(), model.getCode_number(), model.getImage(), model.getName(), "true", model.getItemId(), model.getDescription());
                                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(model.getItemId()).setValue(items);

                            } else {
                                categoryItemViewHolder.item_favorite.setImageResource(R.drawable.ic_favorite_border);
                                tb_category_items.child(model.getItemId()).child("favorite").setValue("false");
                                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(model.getItemId()).removeValue();
                            }

                            tb_category_items.updateChildren(map);
                            tb_user.updateChildren(map);
                        }
                    });
                }
            };
        category_list.setAdapter(adapter);
    }
}