package com.example.myapplicationfirebase;

import android.util.Log;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class WeightedAriMean<T> {

    private static Map<String, Integer> map = new HashMap<>();
    private static Random random = new Random();
    private static String recommandValue;

    public static String getWeightedAriMean(int[] categoryNum) {
        for (int i=0; i< categoryNum.length; i++) {
            if (categoryNum[i] == 0)
                map.put(String.valueOf(i), 1);
            else
                map.put(String.valueOf(i), categoryNum[i]*25);
        }

        int totalWeight = 0;

        NavigableMap<Integer, String> sortedMap = new TreeMap<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            int weight = entry.getValue();
            String letter = entry.getKey();
            totalWeight += weight;

            sortedMap.put(totalWeight, letter);
        }
        List<String> randomList = genRandomListByWeight(1, sortedMap);
        recommandValue = String.valueOf(randomList.get(0));
        //Log.i("==============recommandValue===========", recommandValue);
        return recommandValue;
    }

    public static final <T> List<T> genRandomListByWeight(int size, NavigableMap<Integer, T> weightTable) {
        List<T> randomList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            randomList.add(getRamdomElementByWeight(weightTable));
        }
        return randomList;
    }


    public static final <V> V getRamdomElementByWeight(NavigableMap<Integer, V> weightTable) {
        Double randomDouble = random.nextDouble() * weightTable.lastKey();
        //Log.i("=============randomDouble=========", String.valueOf(randomDouble));
        return weightTable.ceilingEntry(randomDouble.intValue()).getValue();
    }
}
