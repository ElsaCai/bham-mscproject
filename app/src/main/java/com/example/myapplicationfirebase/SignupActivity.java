package com.example.myapplicationfirebase;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplicationfirebase.Module.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.BreakIterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity {

    private EditText re_name, re_pwd, re_email, re_age;
    private RadioGroup re_sex;
    private Button re_btn;
    private Spinner re_country_droplist;
    String couontryValue = "Australia";
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        re_name = (EditText) findViewById(R.id.re_name);
        re_pwd = (EditText) findViewById(R.id.re_pwd);
        re_email = (EditText) findViewById(R.id.re_email);
        re_age = (EditText) findViewById(R.id.re_age);
        re_sex = (RadioGroup) findViewById(R.id.re_sex);
        re_btn = (Button) findViewById(R.id.re_btn);
        re_country_droplist = (Spinner) findViewById(R.id.re_country_droplist);
        
//<======================= User Sign Up Function =======================>
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference db_user = database.getReference("User");

        final String[] items = new String[]{"Australia", "Canada", "China", "France",
                "Germany", "India", "Japan", "Spain", "Taiwan", "United Kingdom", "United States"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        re_country_droplist.setAdapter(adapter);

        re_country_droplist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Log.v("item", re_country_droplist.getSelectedItem());
                couontryValue = items[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        re_btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                final ProgressDialog mDialog = new ProgressDialog(SignupActivity.this);
                mDialog.setMessage("Please waiting...");
                mDialog.show();

                db_user.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if(snapshot.child(re_name.getText().toString()).exists()) {
                            mDialog.dismiss();
                            Toast toast = Toast.makeText(SignupActivity.this, "User account already exists.", Toast.LENGTH_SHORT);
                            ViewGroup group = (ViewGroup) toast.getView();
                            TextView messageTextView = (TextView) group.getChildAt(0);
                            messageTextView.setTextSize(20);
                            toast.show();
                        } else if (! isEmailValid(re_email.getText().toString())) {
                            mDialog.dismiss();
                            Toast toast = Toast.makeText(SignupActivity.this, "Please input the correct format of email.", Toast.LENGTH_SHORT);
                            ViewGroup group = (ViewGroup) toast.getView();
                            TextView messageTextView = (TextView) group.getChildAt(0);
                            messageTextView.setTextSize(20);
                            toast.show();
                        } else {
                            mDialog.dismiss();
                            String get_sex;
                            switch(re_sex.getCheckedRadioButtonId()){
                                case R.id.male:
                                    get_sex = "male";
                                    break;
                                case R.id.female:
                                    get_sex = "female";
                                    break;
                                default:
                                    throw new IllegalStateException("Unexpected value: " + re_sex.getId());
                            }

                            User user = new User(re_name.getText().toString(), re_pwd.getText().toString(), re_email.getText().toString(), get_sex, couontryValue, re_age.getText().toString());
                            db_user.child(re_name.getText().toString()).setValue(user);
                            Toast toast = Toast.makeText(SignupActivity.this, "Sign up successfully.", Toast.LENGTH_SHORT);
                            ViewGroup group = (ViewGroup) toast.getView();
                            TextView messageTextView = (TextView) group.getChildAt(0);
                            messageTextView.setTextSize(20);
                            toast.show();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
//<======================= User Sign Up Function End =======================>
    }

    private boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        if(matcher.matches())
            return true;
        else
            return false;
    }
}