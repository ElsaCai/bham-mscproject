package com.example.myapplicationfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.myapplicationfirebase.CouponData;

import java.io.IOException;

//import com.google.android.gms.common.internal.service.Common;

/**
 * This is Login page
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//====================CALL COUPON !!!!============================
        try {
            CouponData couponData = new CouponData();
            couponData.loadCouponData();
            //Log.i("=========MainActivity==========", "Call CouponData");
        } catch (IOException e) {
            Log.i("=========MainActivity==========", "Call CouponData IOException");
            e.printStackTrace();
        }

//====================CALL CREDIT CARD !!!!============================
        CreditCardInfo creditCardInfo = new CreditCardInfo();
        creditCardInfo.loadCreditCardData();


        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final Button btnSignUp = findViewById(R.id.btnSignUp);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
// <======================= Go To Sign Up Page =======================>
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //loadingProgressBar.setVisibility(View.VISIBLE);
            //loginViewModel.login(usernameEditText.getText().toString(),
            //        passwordEditText.getText().toString());
                Intent intent = new Intent(MainActivity.this, SignupActivity.class);
                startActivity(intent);
            }
        });
// <======================= Go To Sign Up Page End =======================>

// <======================= Login Function =======================>
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference tb_user = database.getReference("User");

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
                mDialog.setMessage("Please waiting...");
                mDialog.show();

                tb_user.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        //String value = snapshot.getValue(String.class);
                        //Log.d(TAG, "Value is: " + value);
                        if(snapshot.child(usernameEditText.getText().toString()).exists()) {
                            //Get User
                            mDialog.dismiss();
                            User user = snapshot.child(usernameEditText.getText().toString()).getValue(User.class);
                            //Log.i("===========User: ",user.getUser_name());
                            if (user.getPwd().equals(passwordEditText.getText().toString())) {
                                Common.currentUser = user;
                                Toast.makeText(MainActivity.this, "Sign in successfully.", Toast.LENGTH_SHORT).show();

                                //Go To Main Page
                                Intent intent = new Intent(MainActivity.this, NavigationActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(MainActivity.this, "Sign in failed.", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(MainActivity.this, "User not exists.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        //Log.w(TAG, "Failed to read value.", error.toException());
                    }
                });
                //loadingProgressBar.setVisibility(View.VISIBLE);
                //loginViewModel.login(usernameEditText.getText().toString(),
                //        passwordEditText.getText().toString());

            }
        });
// <======================= Login Function End =======================>

       /* FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       // if (id == R.id.action_settings) {
       //     return true;
        //}

        return super.onOptionsItemSelected(item);
    }
}