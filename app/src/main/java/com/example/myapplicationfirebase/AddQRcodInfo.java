package com.example.myapplicationfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.example.myapplicationfirebase.ViewHolder.MyCouponItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class AddQRcodInfo extends AppCompatActivity {

    TextView qrcode_info;
    EditText qr_description;
    private Button save_qrcode_info;
    String categoryId = "0";
    FirebaseDatabase database;
    DatabaseReference tb_user;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_q_rcod_info);

        save_qrcode_info = (Button) findViewById(R.id.save_qrcode_info);
        qr_description = (EditText) findViewById(R.id.qr_description);
        Intent intent = getIntent();
        final String str = intent.getStringExtra("qrCodeInfo");

        qrcode_info = (TextView) findViewById(R.id.qrcode_info);
        qrcode_info.setText(str);

        //get the spinner from the xml.
        final Spinner dropdown = findViewById(R.id.category_droplist);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Log.v("item", String.valueOf(position));
                categoryId = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        //Set to My Coupon of Firebase
        database = FirebaseDatabase.getInstance();
        tb_user = database.getReference("User");

        save_qrcode_info.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String id = UUID.randomUUID().toString();
                CategoryItems items = new CategoryItems(categoryId,
                                                        str,
                                                "coupon",
                                                        qr_description.getText().toString(),
                                                "true",
                                                        id,
                                            "");
                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(id).setValue(items);
                Toast.makeText(AddQRcodInfo.this, "Add successfully!", Toast.LENGTH_SHORT).show();

                //Intent intent = new Intent(AddQRcodInfo.this, ScanBarCode.class);
                //startActivity(intent);
            }
        });

        //create a list of items for the spinner. { "womens-fashion", "mens-fashion", "home-and-garden", "electronics-and-cell-phones", "food-and-drink", "travel", "pets", "entertainment"}
        String[] items = new String[]{"Women's Fashion", "Men's Fashion", "Home and Garden", "Electronics and Cell Phones",
                "Food and Drink", "Travel", "Pets", "Entertainment"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

    }

}