package com.example.myapplicationfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.example.myapplicationfirebase.Module.CommunityItems;
import com.example.myapplicationfirebase.Module.User;
import com.example.myapplicationfirebase.ViewHolder.CategoryItemViewHolder;
import com.example.myapplicationfirebase.ViewHolder.CommunityItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CommunityDetailReView extends AppCompatActivity {
    private CommunityItems items = new CommunityItems();
    private RecyclerView.LayoutManager layoutManager;
    String postId = "";
    FirebaseDatabase database;
    DatabaseReference tb_community_item;
    DatabaseReference tb_community;
    DatabaseReference tb_user;
    RecyclerView community_detail_list;
    FirebaseRecyclerAdapter<CommunityItems, CommunityItemViewHolder> adapter;
    TextView title, de_username;
    TextView description;
    private Button reply_btn;
    Dialog popAddPost;
    String main_topic;
    Long community_item_num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_detail_re_view);

        community_detail_list = (RecyclerView) findViewById(R.id.community_detail_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_community_item = database.getReference("Community Items");
        tb_community = database.getReference("Community");
        tb_user = database.getReference("User");
        layoutManager = new LinearLayoutManager(this);
        community_detail_list.setLayoutManager(layoutManager);

        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        de_username = findViewById(R.id.de_username);
        reply_btn = (Button) findViewById(R.id.reply_btn);

        //Get Community Item by communityId
        if(getIntent() != null)
            postId = getIntent().getStringExtra("postId");
        if(!postId.isEmpty() && postId != null) {
            getCommunityItem(postId);
            //Retrieve the Community Topic and Description
            tb_community.child(postId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    // data available in snapshot.value()
                    CommunityItems item = snapshot.getValue(CommunityItems.class);
                    String titleValue = item.getTitle();
                    String descriptionValue = item.getContent();
                    String de_usernameValue = item.getUsername();
                    title.setText(titleValue);
                    description.setText(descriptionValue);
                    de_username.setText(de_usernameValue);
                    main_topic = titleValue;
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        iniPost();
        reply_btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                popAddPost.show();
                final TextView add_title = popAddPost.findViewById(R.id.add_title);
                add_title.setText(main_topic);
                add_title.setEnabled(false);
                ImageView post = (ImageView) popAddPost.findViewById(R.id.post);
                tb_community_item.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        community_item_num = snapshot.getChildrenCount()+1;
                        //Log.i("=============Length ==", String.valueOf(snapshot.getChildrenCount()));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
                post.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        TextView add_username = popAddPost.findViewById(R.id.add_username);
                        TextView add_description = popAddPost.findViewById(R.id.add_description);

                        //int new_postId = Integer.parseInt(postId) + 1;
                        long new_postId = new Date().getTime()-1;

                        final CommunityItems communityItems = new CommunityItems(add_title.getText().toString(), add_description.getText().toString(), add_username.getText().toString(), String.valueOf(community_item_num));
                        tb_community_item.child(String.valueOf(community_item_num)).setValue(communityItems);
                        tb_community_item.child(String.valueOf(community_item_num)).child("communityId").setValue(postId);
                        popAddPost.dismiss();
                    }
                });
            }
        });
    }

    private void getCommunityItem(String postId) {
        adapter = new FirebaseRecyclerAdapter<CommunityItems, CommunityItemViewHolder>(
                CommunityItems.class,
                R.layout.activity_community_detail,
                CommunityItemViewHolder.class,
                tb_community_item.orderByChild("communityId").equalTo(postId)) {
            @Override
            protected void populateViewHolder(final CommunityItemViewHolder communityItemViewHolder, final CommunityItems model, final int i) {
                communityItemViewHolder.community_item_username.setText(model.getUsername());
                communityItemViewHolder.body.setText(model.getContent());
            }
        };
        community_detail_list.setAdapter(adapter);
    }

    private void iniPost() {
        popAddPost = new Dialog(this);
        popAddPost.setContentView(R.layout.community_addpost);
        //popAddPost.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popAddPost.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popAddPost.getWindow().getAttributes().gravity = Gravity.TOP;
    }
}