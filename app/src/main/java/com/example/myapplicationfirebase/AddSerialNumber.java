package com.example.myapplicationfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

public class AddSerialNumber extends AppCompatActivity {
    EditText serial_num;
    EditText serial_num_description;
    private Button save_num_info;
    String categoryId = "0";
    FirebaseDatabase database;
    DatabaseReference tb_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_serial_number);

        serial_num = (EditText) findViewById(R.id.serial_num);
        serial_num_description = (EditText) findViewById(R.id.serial_num_description);
        save_num_info = (Button) findViewById(R.id.save_num_info);
        //Set to My Coupon of Firebase
        database = FirebaseDatabase.getInstance();
        tb_user = database.getReference("User");

        //get the spinner from the xml.
        final Spinner dropdown = findViewById(R.id.category_droplist);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Log.v("item", String.valueOf(position));
                categoryId = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        //create a list of items for the spinner. { "womens-fashion", "mens-fashion", "home-and-garden", "electronics-and-cell-phones", "food-and-drink", "travel", "pets", "entertainment"}
        String[] items = new String[]{"Women's Fashion", "Men's Fashion", "Home and Garden", "Electronics and Cell Phones",
                "Food and Drink", "Travel", "Pets", "Entertainment"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        save_num_info.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String id = UUID.randomUUID().toString();
                CategoryItems items = new CategoryItems(categoryId,
                        serial_num.getText().toString(),
                        "coupon",
                        serial_num_description.getText().toString(),
                        "true",
                        id,
                        "");
                tb_user.child(Common.currentUser.getUser_name()).child("favorite_list").child(id).setValue(items);
                Toast.makeText(AddSerialNumber.this, "Add successfully!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}