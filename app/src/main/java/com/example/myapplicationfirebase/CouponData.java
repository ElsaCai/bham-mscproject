package com.example.myapplicationfirebase;

import android.util.Log;

import com.example.myapplicationfirebase.Module.CategoryItems;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Get Coupon Data from https://www.promocodes.com
 */
public class CouponData {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference db_category_items = database.getReference("Category Items");
    int itemId = 0;
    String[] stringArray = { "womens-fashion", "mens-fashion", "home-and-garden", "electronics-and-cell-phones", "food-and-drink", "travel", "pets", "entertainment"};
    public void loadCouponData() throws IOException {
        for(int l=0; l<stringArray.length; l++) {
            loadWomenFashionData(stringArray[l], l);
        }
    }


    public void loadWomenFashionData(final String s, final int categoryId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                JSONArray jsonArray = new JSONArray();
                //Log.i("===========loadCouponData======", "Into loadCouponData function");
                try {
                    //==================== Women's Fashion ====================
                    int i = 0, j=0;
                    Document doc_womens_fashion = Jsoup.connect("https://www.promocodes.com/coupons/"+s).get();
                    //Get Brand Name
                    Elements el_womens_fashion_logo = doc_womens_fashion.select("div.has-code > article.coupon > div.coupon-header > a.coupon-logo > img");
                    for (Element element : el_womens_fashion_logo) {
                        String brand_name = element.attr("alt").replace("logo", "");
                        String brand_img = element.attr("data-src");
                        //Log.i("=====data-src====", brand_img);
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("brand_name", brand_name);
                        jsonObject.put("brand_img", brand_img);
                        jsonArray.put(jsonObject);
                        i++;
                        if(i==10) break;
                    }
                    //Get link id
                    Elements el_link_id = doc_womens_fashion.select("a[aria-label=See promo code]");
                    for (Element element : el_link_id) {
                        String id = element.attr("data-coupon-id");
                        //Log.i("=====id====", id);
                        //Get Promo Code
                        Document doc_womens_fashion_id = Jsoup.connect("https://www.promocodes.com/couponmodals/coupon?couponId="+id).get();
                        Elements el_code_womens_fashion = doc_womens_fashion_id.select("div[class=coupon-modal-code]");
                        String code_womens_fashion = el_code_womens_fashion.text();
                        JSONObject var1 = jsonArray.getJSONObject(j);
                        var1.put("code_womens_fashion", code_womens_fashion);
                        //Log.i("=====code_womens_fashion====", code_womens_fashion);
                        Elements el_des_womens_fashion = doc_womens_fashion_id.select("span[class=coupon-headline-savings]");
                        String description = el_des_womens_fashion.text();
                        var1.put("description", description);
                        //Log.i("=====description====", description);
                        j++;
                        if(j==10) break;
                    }

                    for (int n = 0; n < jsonArray.length(); n++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(n);
                        String brand_name = jsonObject.getString("brand_name");
                        String code_womens_fashion = jsonObject.getString("code_womens_fashion");
                        String description = jsonObject.getString("description");
                        String brand_img = jsonObject.getString("brand_img");
                        //Log.d("TAG>>>>>>>>>>>>>>>>", "brand_name:" + brand_name + ", code_womens_fashion:" + code_womens_fashion + ", description:" + description);

                        //categoryId, code_number, image, name, favorite, itemId
                        CategoryItems items = new CategoryItems(
                                String.valueOf(categoryId),
                                code_womens_fashion,
                                brand_img,
                                brand_name,
                                "false",
                                String.valueOf(itemId),
                                description);
                        db_category_items.child(String.valueOf(itemId)).setValue(items);
                        itemId++;
                    }


                    //Log.i("============categoryId======", String.valueOf(categoryId));
                } catch (IOException | JSONException e) {
                    //Log.i("===========title Error======", Objects.requireNonNull(e.getMessage()));
                }
            }
        }).start();
    }
}
