package com.example.myapplicationfirebase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.CategoryItems;
import com.example.myapplicationfirebase.Module.CreditCardInfoItems;
import com.example.myapplicationfirebase.ViewHolder.CreditCardItemViewHolder;
import com.example.myapplicationfirebase.ViewHolder.MyCouponItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class CreditCardList extends AppCompatActivity {
    private RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference tb_credit_items;
    RecyclerView card_list;
    FirebaseRecyclerAdapter<CreditCardInfoItems, CreditCardItemViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_list);

        card_list = (RecyclerView) findViewById(R.id.card_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_credit_items = database.getReference("Credit Items");
        layoutManager = new LinearLayoutManager(this);
        card_list.setLayoutManager(layoutManager);

        layoutManager = new LinearLayoutManager(this);
        card_list.setLayoutManager(layoutManager);
        //my_coupon_list.setAdapter(myCouponAdapter);

        getCreditList();

    }

    private void getCreditList() {
        adapter = new FirebaseRecyclerAdapter<CreditCardInfoItems, CreditCardItemViewHolder>(
                CreditCardInfoItems.class,
                R.layout.activity_credit_card_items,
                CreditCardItemViewHolder.class,
                tb_credit_items) {
            @Override
            protected void populateViewHolder(CreditCardItemViewHolder creditCardItemViewHolder, final CreditCardInfoItems model, int i) {
                creditCardItemViewHolder.card_name.setText(model.getCard_name());
                creditCardItemViewHolder.card_info.setText(model.getCard_info());

                if(model.getCard_img().indexOf("https") == 0) {
                    Picasso.with(getBaseContext()).load(model.getCard_img()).into(creditCardItemViewHolder.card_img);
                } else {
                    //int resId = getResources().getIdentifier(model.getImage(), "drawable", getPackageName());
                    //myCouponItemViewHolder.item_img.setImageResource(resId);
                }
            }
        };
        card_list.setAdapter(adapter);
    }
}