package com.example.myapplicationfirebase;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Module.Category;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private ArrayList<Category> products;

    private Context context;

    private LayoutInflater layoutInflater;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageView;
        public ViewHolder(@NonNull View itemView /*TextView v*/) {
            super(itemView);
            //Log.i("UI >>>>>>>>>>>>>> v", "CategoryAdapter ViewHolder = "+itemView);
            textView = (TextView) itemView.findViewById(R.id.txt_category_name);
            imageView = (ImageView) itemView.findViewById(R.id.list_image);
        }
    }

    public CategoryAdapter(ArrayList<Category> products/*, Context context*/) {
        this.products = products;
        //this.context = context;
        //layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*ViewHolder vh = new ViewHolder(new TextView(parent.getContext()));
        vh.textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80f);
        Log.i("UI >>>>>>>>>>>>>>", "CategoryAdapter CategoryAdapter.ViewHolder onCreateViewHolder");
        return vh;*/

        View itemView = LayoutInflater.from(context).inflate(R.layout.coupon_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {

        final Category pr = (Category) products.get(position);
        //holder.imageView.setImageResource(pr.getImage());
        holder.textView.setText(pr.getName());

        //Log.i("UI >>>>>>>>>>>>>>product", "CategoryAdapter onBindViewHolder = "+products.get(position).toString());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

}
