package com.example.myapplicationfirebase;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationfirebase.Interface.ItemClickListener;
import com.example.myapplicationfirebase.Module.CommunityItems;
import com.example.myapplicationfirebase.ViewHolder.CommunityListViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class CommunityActivity extends AppCompatActivity {
    //private ArrayList<CommunityItems> communityList = new ArrayList<CommunityItems>();
    //private CommunityAdapter communityAdapter;
    private RecyclerView.LayoutManager layoutManager;
    FirebaseRecyclerAdapter<CommunityItems, CommunityListViewHolder> adapter;

    FirebaseDatabase database;
    DatabaseReference tb_community;
    RecyclerView community_list;
    Long postId;

    //public ImageView post;

    Dialog popAddPost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community);

        setContentView(R.layout.activity_community);
        community_list = (RecyclerView) findViewById(R.id.community_list);
        //Init Firebase
        database = FirebaseDatabase.getInstance();
        tb_community = database.getReference("Community");

        layoutManager = new LinearLayoutManager(this);
        community_list.setLayoutManager(layoutManager);

        getCommunityList();

        FloatingActionButton add_post = (FloatingActionButton) findViewById(R.id.add_post);
        iniPost();
        add_post.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                popAddPost.show();

                //Dialog Form Function https://stackoverflow.com/questions/38476045/android-custom-dialog-with-button-onclick-event
                ImageView post = (ImageView) popAddPost.findViewById(R.id.post);
                post.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view) {
                        TextView add_username = popAddPost.findViewById(R.id.add_username);
                        TextView add_title = popAddPost.findViewById(R.id.add_title);
                        TextView add_description = popAddPost.findViewById(R.id.add_description);

                        //int new_postId = Integer.parseInt(postId) + 1;
                        long new_postId = new Date().getTime()-1;

                        final CommunityItems communityItems = new CommunityItems(add_title.getText().toString(), add_description.getText().toString(), add_username.getText().toString(), String.valueOf(new_postId));
                        tb_community.child(String.valueOf(new_postId)).setValue(communityItems);

                        popAddPost.dismiss();
                    }
                });
            }
        });
    }

    private void getCommunityList() {
        adapter = new FirebaseRecyclerAdapter<CommunityItems, CommunityListViewHolder>(
                CommunityItems.class, R.layout.community_item, CommunityListViewHolder.class, tb_community.orderByChild("postId")) {
            @Override
            protected void populateViewHolder(CommunityListViewHolder communityListViewHolder, final CommunityItems model, int i) {
                communityListViewHolder.community_item_topic.setText(model.getTitle());
                communityListViewHolder.community_item_content.setText(model.getContent());
                communityListViewHolder.community_item_username.setText(model.getUsername());
                //postId = model.getPostId();

                final  CommunityItems clickItem = model;
                communityListViewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent activity_community_detail_re_view = new Intent(CommunityActivity.this, CommunityDetailReView.class);
                        activity_community_detail_re_view.putExtra("postId", model.getPostId());
                        startActivity(activity_community_detail_re_view);
                    }
                });
            }
        };
        community_list.setAdapter(adapter);
    }

    private void iniPost() {
        popAddPost = new Dialog(this);
        popAddPost.setContentView(R.layout.community_addpost);
        //popAddPost.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popAddPost.getWindow().setLayout(Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.WRAP_CONTENT);
        popAddPost.getWindow().getAttributes().gravity = Gravity.TOP;
    }
}