package com.example.myapplicationfirebase;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplicationfirebase.Common.Common;
import com.example.myapplicationfirebase.Module.Upload;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class NavigationActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    TextView nav_username;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        View headerView = navigationView.getHeaderView(0);
        nav_username = (TextView) headerView.findViewById(R.id.nav_username);
        nav_username.setText(Common.currentUser.getEmail());

//Get image from firebase

        //mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads");
        //mStorage= FirebaseStorage.getInstance();
        //StorageReference mStorageRef = mStorage.getReferenceFromUrl("gs://myfirebasereadwritedatatest.appspot.com");
        imageView = (ImageView) headerView.findViewById(R.id.imageView);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://myfirebasereadwritedatatest.appspot.com");
        Task<Uri> uri = storageRef.child("uploads/1597208283346.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // TODO: handle uri
                //Log.i("-------------url:", String.valueOf(uri));
                Picasso.with(NavigationActivity.this).load(uri.toString()).into(imageView);
            }
        });
         //   StorageReference spaceRef = mStorageRef.child("uploads/1597208283346.jpg");
         //   Task<Uri> url = spaceRef.getDownloadUrl();
        StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl("gs://myfirebasereadwritedatatest.appspot.com/uploads/");
        /*storageReference.child("uploads").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                //do your stuff- uri.toString() will give you download URL\\
                //Log.i("-------------url:", String.valueOf(uri));
                //Picasso.with(NavigationActivity.this).load(uri.toString()).into(imageView);
            }
        });*/
            //System.out.println("Here is the print of url : "+url);
            //Picasso.with(NavigationActivity.this).load(String.valueOf(url))
            //        .error(R.mipmap.ic_launcher).
            //        resize(50, 50)
            //        .placeholder(R.mipmap.ic_launcher).into(imageView);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}