import pyrebase
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, f_regression, chi2
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import statsmodels.api as sm
import tabulate


config = {
  "apiKey": "AIzaSyCbOi0hKCuf9T34ekMMEWvNZW_8JzEh1VI",
  "authDomain": "myfirebasereadwritedatatest.firebaseapp.com",
  "databaseURL": "https://myfirebasereadwritedatatest.firebaseio.com",
  "storageBucket": "myfirebasereadwritedatatest.appspot.com"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()
user_name_list = db.child('User').shallow().get().val()

category = [0, 1, 2, 3, 4, 5, 6, 7]
category_id = []
age = []
sex = []
favorite = []

country=[]
label_encoder=LabelEncoder()
for unser_name in user_name_list:
    user = db.child("User").child(unser_name).get()
    fav = db.child("User").child(unser_name).child("favorite_list").get()
    cu_age = float(user.val()['age'])
    cu_sex = user.val()['sex']
    #cu_country = user.val()['country']
    #print(cu_country)
    for fav in fav.each():
        #print(fav.val()['categoryId'])
        fav_category = [0, 0, 0, 0, 0, 0, 0, 0]
        fav_category[int(fav.val()['categoryId'])] = 1
    for i, v in enumerate(fav_category):
        category_id.append(float(i))
        age.append(cu_age)
        #country.append(cu_country)
        if cu_sex == 'female':
            gender = 0.0
        else:
            gender = 1.0
        sex.append(gender)
        if v == 0:
            favorite.append(0)
        else:
            favorite.append(1)


#c=label_encoder.fit_transform(country)
print(category_id)
print('-------------------------------')
print(age)
print('-------------------------------')
print(sex)
print('-------------------------------')
print(favorite)
print('-------------------------------')


x = pd.DataFrame([category_id, sex, age]).T
x.columns = ["Category Id", "Gender", "Age"]
#target = pd.DataFrame(favorite, columns=["Favorite"])
#target = pd.DataFrame([favorite])
#y = target["favorite"]

logistic = LogisticRegression(class_weight='balanced')
target = pd.DataFrame(favorite, columns=["Favorite"])
y = target["Favorite"]
logistic.fit(x, y)
print(y)

print(tabulate.tabulate(x, headers='keys', tablefmt='psql', showindex=True))

print("regration va = ", logistic.coef_)
print("dis = ", logistic.intercept_)

print(f_regression(x, y)[1]) #Logistic 迴歸模型我們也可以檢定變數的顯著性，以 P-value 是否小於 0.05（信心水準 95%）來判定。
print('-------------------------------')
pred = logistic.predict(x)
print("pred : ", pd.crosstab(pred, favorite))
print('-------------------------------')


new_fav = pd.DataFrame(np.array([[7, 0, 34]]))
predict = logistic.predict(x)
print("predict :", predict)
plt.plot(x, predict, 'ro')
plt.show()

print('-------------------------------')
#print(SelectKBest(f_regression, k=3))
print("accuracy : ", logistic.score(x, favorite))
#P-value inifo
logit_model=sm.Logit(favorite, x)
result=logit_model.fit()
print(result.summary())
