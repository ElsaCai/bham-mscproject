import pyrebase
import numpy as np
import matplotlib.pyplot as plt


config = {
  "apiKey": "AIzaSyCbOi0hKCuf9T34ekMMEWvNZW_8JzEh1VI",
  "authDomain": "myfirebasereadwritedatatest.firebaseapp.com",
  "databaseURL": "https://myfirebasereadwritedatatest.firebaseio.com",
  "storageBucket": "myfirebasereadwritedatatest.appspot.com"
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()

#for user in user.each():
#    print(user.key()) # Morty
#    print(user.val()) # {name": "Mortimer 'Morty' Smith"}
category_id = [0, 1, 2, 3, 4, 5, 6, 7] #X axis
sex = []
favorite = []
male_fa_num = [] #Y1 axis
female_fa_num = [] #Y2 axis
male_fa = []
female_fa = []
user_name_list = db.child('User').shallow().get().val()
for unser_name in user_name_list:
    user = db.child("User").child(unser_name).get()
    fav = db.child("User").child(unser_name).child("favorite_list").get()
    for fav in fav.each():
        #print(fav.val()['categoryId'])
        favorite.append(int(fav.val()['categoryId']))
        if user.val()['sex'] == 'female':
            gender = 0
            female_fa.append(int(fav.val()['categoryId']))
        else:
            gender = 1
            male_fa.append(int(fav.val()['categoryId']))
        sex.append(gender)
print(sex)
print(favorite)

'''Bar chart'''
l = list(female_fa)
lm = list(male_fa)
female_fa_num = np.array([l.count(0), l.count(1), l.count(2), l.count(3), l.count(4), l.count(5), l.count(6), l.count(7)])
male_fa_num = np.array([lm.count(0), lm.count(1), lm.count(2), lm.count(3), lm.count(4), lm.count(5), lm.count(6), lm.count(7)])
print(female_fa_num)
print(male_fa_num)

width = 0.4
plt.bar([i-width/2 for i in category_id], female_fa_num, width=width, label='Female favorite')
plt.bar([i+width/2 for i in category_id], male_fa_num, width=width, label='Male favorite')
plt.xlabel("Category Id")
plt.ylabel("Favorite Number")
plt.legend()
'''Bar chart end'''



plt.show()
