import pyrebase
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import SelectKBest, f_regression, chi2
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import statsmodels.api as sm
import tabulate
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import neighbors
from sklearn import cluster
import mglearn

config = {
  "apiKey": "AIzaSyCbOi0hKCuf9T34ekMMEWvNZW_8JzEh1VI",
  "authDomain": "myfirebasereadwritedatatest.firebaseapp.com",
  "databaseURL": "https://myfirebasereadwritedatatest.firebaseio.com",
  "storageBucket": "myfirebasereadwritedatatest.appspot.com"
}

firebase = pyrebase.initialize_app(config)
db = firebase.database()
user_name_list = db.child('User').shallow().get().val()

category = [0, 1, 2, 3, 4, 5, 6, 7]
category_id = []
age = []
sex = []
favorite = []

country=[]
label_encoder=LabelEncoder()
for unser_name in user_name_list:
    user = db.child("User").child(unser_name).get()
    fav = db.child("User").child(unser_name).child("favorite_list").get()
    cu_age = float(user.val()['age'])
    cu_sex = user.val()['sex']
    #cu_country = user.val()['country']
    #print(cu_country)
    for fav in fav.each():
        #print(fav.val()['categoryId'])
        fav_category = [0, 0, 0, 0, 0, 0, 0, 0]
        fav_category[int(fav.val()['categoryId'])] = 1
    for i, v in enumerate(fav_category):
        category_id.append(float(i))
        age.append(cu_age)
        #country.append(cu_country)
        if cu_sex == 'female':
            gender = 0.0
        else:
            gender = 1.0
        sex.append(gender)
        if v == 0:
            favorite.append(0)
        else:
            favorite.append(1)


#c=label_encoder.fit_transform(country)
print(category_id)
print('-------------------------------')
print(age)
print('-------------------------------')
print(sex)
print('-------------------------------')
print(favorite)
print('-------------------------------')


#x = pd.DataFrame([category_id, sex, age]).T
#x.columns = ["Category Id", "Gender", "Age"]
x = pd.DataFrame({
    "category_id": category_id,
    "Gender":   sex,
    "Age":  age
})

#target = pd.DataFrame(favorite, columns=["Favorite"])
#target = pd.DataFrame([favorite])
#y = target["favorite"]

logistic = LogisticRegression(class_weight='balanced')
target = pd.DataFrame(favorite, columns=["Favorite"])
y = target["Favorite"]

X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.4, random_state=4)
print("----------- X_train ----------- ")
print(X_train)
print("----------- X_test ----------- ")
print(X_test)
print("----------- y_train ----------- ")
print(y_train)
print("----------- y_test ----------- ")
print(y_test)
k = 5

knn = neighbors.KNeighborsClassifier(n_neighbors=k)
knn.fit(X_train, y_train)
pred = knn.predict(X_test)
#print(pred)
print("accuracy : ",accuracy_score(y_test, pred))

training_accuracy = []
test_accuracy = []
#K value is from 1-8
k_setting = range(1, 8)
for n_neighbors in k_setting:
    clf = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors)
    clf.fit(X_train, y_train)
    training_accuracy.append(clf.score(X_train, y_train))
    test_accuracy.append(clf.score(X_test, y_test))
    knn_all = neighbors.KNeighborsClassifier(n_neighbors=n_neighbors)
    knn_all.fit(X_train, y_train)
    pred = knn_all.predict(X_test)
    print("Accuracy Value in each K = ", n_neighbors, " : ", accuracy_score(y_test, pred))

plt.plot(k_setting, training_accuracy, label="Training data accuracy")
plt.plot(k_setting, test_accuracy, label="Test data accuracy")

plt.ylabel("Accuracy")
plt.xlabel("N neighbors")
plt.legend()
plt.show()
