import pyrebase
import matplotlib.pyplot as plt


config = {
  "apiKey": "AIzaSyCbOi0hKCuf9T34ekMMEWvNZW_8JzEh1VI",
  "authDomain": "myfirebasereadwritedatatest.firebaseapp.com",
  "databaseURL": "https://myfirebasereadwritedatatest.firebaseio.com",
  "storageBucket": "myfirebasereadwritedatatest.appspot.com"
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()

#for user in user.each():
#    print(user.key()) # Morty
#    print(user.val()) # {name": "Mortimer 'Morty' Smith"}
category_id = [0, 1, 2, 3, 4, 5, 6, 7] #X axis

male_fa = []
female_fa = []
male_age = []
female_age = []
user_name_list = db.child('User').shallow().get().val()
for unser_name in user_name_list:
    user = db.child("User").child(unser_name).get()
    fav = db.child("User").child(unser_name).child("favorite_list").get()
    for fav in fav.each():
        #print(fav.val()['categoryId'])
        #favorite.append(int(fav.val()['categoryId']))
        if user.val()['sex'] == 'female':
            gender = 0
            female_fa.append(int(fav.val()['categoryId']))
            female_age.append(int(user.val()['age']))
        else:
            gender = 1
            male_fa.append(int(fav.val()['categoryId']))
            male_age.append(int(user.val()['age']))


plt.scatter(female_fa, female_age, label='Female Favorite', marker='^', c='r', s=[100 for s in range(len(category_id))], alpha=0.5)
plt.scatter(male_fa, male_age, label='Male Favorite', marker='s', c='c', s=[100 for s in range(len(category_id))], alpha=0.5)
plt.title("Gender-Age-Preference")
plt.xlabel("Category Id")
plt.ylabel("Age")
plt.show()

