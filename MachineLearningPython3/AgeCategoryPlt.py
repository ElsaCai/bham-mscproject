import pyrebase
import matplotlib.pyplot as plt


config = {
  "apiKey": "AIzaSyCbOi0hKCuf9T34ekMMEWvNZW_8JzEh1VI",
  "authDomain": "myfirebasereadwritedatatest.firebaseapp.com",
  "databaseURL": "https://myfirebasereadwritedatatest.firebaseio.com",
  "storageBucket": "myfirebasereadwritedatatest.appspot.com"
}

firebase = pyrebase.initialize_app(config)

db = firebase.database()

#for user in user.each():
#    print(user.key()) # Morty
#    print(user.val()) # {name": "Mortimer 'Morty' Smith"}
category_id = [0, 1, 2, 3, 4, 5, 6, 7] #X axis
age = []
favorite = []
user_name_list = db.child('User').shallow().get().val()
for unser_name in user_name_list:
    user = db.child("User").child(unser_name).get()
    fav = db.child("User").child(unser_name).child("favorite_list").get()
    for fav in fav.each():
        #print(fav.val()['categoryId'])
        favorite.append(int(fav.val()['categoryId']))
        age.append(int(user.val()['age']))
print(age)
print(favorite)

plt.scatter(favorite, age, s=[100 for i in range(len(favorite))],c='#000000',alpha=0.5)
plt.title("Age-Preference")
plt.xlabel("Category Id")
plt.ylabel("Age")
plt.show()


plt.show()
